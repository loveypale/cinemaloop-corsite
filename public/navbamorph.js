//

function navmorph(){
  var dalink = document.getElementsByClassName('nav-link');
  var navbar = document.getElementById("navnav");
  var Y = window.pageYOffset;
  var W = window.innerWidth;
  var logo_image = document.getElementById("logo_sm");
  var bar = document.querySelector('.fixed-top');
  var jumpup = document.querySelector('.back-up');
  var downscroll = 150;
  var wscreen = 768;
  var i;

  // We set logic for when user is scrolling down
  if( Y >= downscroll && W >= wscreen){
    
    //this loop  searches the DOM  for navigation links and changes the 
    //the text color relative to the background color.
    for(i = 0; i < dalink.length; i++){
      dalink[i].style.cssText += "color: #222222!important;"
    }
     navbar.style.cssText += "background-color: #fff!important;"
     logo_image.src='logotype.png';
     bar.style.top = '0';
    }
  else if(W < wscreen){
     navbar.style.cssText += "background-color: rgb(255, 255, 255)!important;"
     logo_image.src='logotype.png';
     bar.style.top = '0';
   }
  else {
    for(i = 0; i < dalink.length; i++){
      dalink[i].style.cssText += "color: #fff!important;"
    }
     navbar.style.cssText += "background-color: #fff0!important;"
    logo_image.src='logotype_white.png';
    bar.style.top = '1.5rem';
    jumpup.style.visibility ="hidden";
  }
 }
