import React, { Component } from 'react';
import './App.css';
import './index.css';
import {map} from './index.js';
class A1 extends Component {
  render() {
    return (
      <div class="row">
        <p id="map"></p>
        <div id="A" class="col">
          <h2>Where we @</h2>
          <p>4 Continents, 30 members, 17 nationalities</p>
          <div id="map">
            <img class="map" width="70%" src={map} alt="where we at" />       
          </div>
        </div>
      </div>
    );
  }
}

export default A1;
