import React, { Component } from 'react';
import './App.css';
import './index.css';

class Contact extends Component {
  render() {
    return (

        <div id="contact-form" class="modal fade"  role="dialog">
          <div class="modal-dialog">

            {/*-- Modal content--*/}
            <div class="modal-content modal-style">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            {/*FORM STARTS HERE*/}
            <form action="https://docs.google.com/forms/d/e/1FAIpQLScyNplMewH6BOjBzZ0bosCVsFgCEVmj5Bt-A8yh8jDB9US_NA/formResponse" method="post">
              <div class="form-group">
                <label for="exampleInputEmail1">Your Name*</label>
                  <input type="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Name" />
                  <br/>
                <label for="exampleInputEmail1">Email address*</label>
                  <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
                  <label for="exampleInputEmail1">Subject*
                  </label>
                  <br/>
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" />
                  <br/>
                  <textarea class="form-control" type="textarea" id="message" name="message" placeholder="Message" maxLength="140" rows="7" required></textarea>

              </div>
              <button type="submit" class="btn btn-dark access">Send</button>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
