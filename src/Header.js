import React, { Component } from 'react';
import './App.css';
import './index.css';

class Header extends Component {
  render() {
    return (
      <div class='row'>
        <p id="home"></p>
          <div id="content_A" class="col-sm-6">
            <h1>Find your movie tribe</h1>
            <p></p>
            <p class="tagline">Cinemaloop is a movie fan community platform that helps moviegoers explore, discuss, connect and meetup. Cinemaloopers can build personalized 'cinema world' experiences, with tailored cinema-going opportunities.</p>
            <br/>
            <button type="button" class="btn btn-dark access my-2 my-sm-0" data-toggle="modal" data-target="#myModal">Early Access</button>
            <button type="button" class="btn btn-outline-dark video-but"  data-toggle="modal" data-target="#video_1">
            <i class="fas fa-play mr-2"></i>
            WATCH VIDEO</button>
          </div>
          <div id="content_B" class="col-sm-6"></div>
      </div>
    );
  }
}

export default Header;
