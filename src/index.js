import './App.css';
import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';
import A from './section_A';
import A1 from './section_A1';
import B from './section_B';
import Email from './email-module';
import Explainer from './explianer-vid.js'
import Footer from './footer.js';
import Contact from './contact-modal.js';
import * as serviceWorker from './serviceWorker';
import Navigation from './navigation.js';
export { default as img01 } from './team/col-1.png';
export { default as img02 } from './team/col-2.png';
export { default as img03 } from './team/col-3.png';
export { default as img04 } from './team/col-4.png';
export { default as img05 } from './team/col-5.png';
export { default as img06 } from './team/col-6.png';
export { default as vera } from './team/boss-lady.jpg';
export { default as map } from './world.svg';
export { default as logotype } from './logotype.png';
export { default as mascot } from './mascot.png';
export { default as phone } from './phone-app.png';

ReactDOM.render(<Header />, document.getElementById('header'));

ReactDOM.render(<Navigation />, document.getElementById('navi'));

ReactDOM.render(<A />, document.getElementById('section_A'));

ReactDOM.render(<A1 />, document.getElementById('section_A1'));

ReactDOM.render(<B />, document.getElementById('section_B'));

ReactDOM.render(<Email />, document.getElementById('signup'));

ReactDOM.render(<Explainer />, document.getElementById('video'));

ReactDOM.render(<Footer />, document.getElementById('footer'));

ReactDOM.render(<Contact />, document.getElementById('contact'));

serviceWorker.unregister();
