import React, { Component } from 'react';
import './App.css';
import './index.css';
import { logotype } from './';
class Footer extends Component {
  render() {
    return (
      <div class='row footer'>
        <div class="col">
          <img class="footmage" src={logotype} alt="logotype" /><br/>
          <a class="foot-nav" href="/#map">About</a>
          <a class="foot-nav" href="#contact-form">Contact</a>
          <a class="foot-nav" href="https://blog.cinemaloop.co/">Blog</a>
          <a class="foot-nav" href="https://angel.co/company/cinemaloop/jobs">Join our team</a>
          <span class="social">
            <a href="https://www.instagram.com/cinemaloop/" class="social-link">
            <i class="fab fa-instagram mr-4 ml-3"></i></a>
            <a href="https://twitter.com/cinemaloop" class="social-link">
            <i class="fab fa-twitter mr-4"></i></a>
            <a href="https://www.facebook.com/cinemaloop" class="social-link">
            <i class="fab fa-facebook-f mr-4"></i></a>
          </span>
          <p>&copy; cinemaloop Inc. 2019</p>
      </div>
    </div>
    );
  }
}

export default Footer;
