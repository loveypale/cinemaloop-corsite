import React, { Component } from 'react';
import './App.css';
import './index.css';
class Email extends Component {
  render() {
    return (

        <div id="myModal" class="modal fade"  role="dialog">
          <div class="modal-dialog">

            {/*-- Modal content--*/}
            <div class="modal-content modal-style">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <form id="subscribeForm" action="https://app.mailerlite.com/webforms/submit/v6f2b6" 
            data-code="v6f2b6"
            method="POST">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input id="emailaddress" type="email" name="fields[email]" class="form-control" aria-describedby="emailHelp" placeholder="Enter email" />
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <button type="submit" class="btn btn-dark access">Submit</button>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Email;
