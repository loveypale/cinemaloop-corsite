import React, { Component } from 'react';
import './App.css';
import './index.css';
import './email-module.js';

class Navigation extends Component {
  render() {
    return (
        
        //Navbar is not rendered by React
    <div id="navMenu">
    <nav id="navnav" className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
<a className="navbar-brand" href="/#">
<img id="logo_sm" src="logotype_white.png" width="200em"  className="tooltip-test" title="Take me home" alt="logo" /></a>
<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span className="navbar-toggler-icon"></span>
</button>

<div className="collapse navbar-collapse" id="navbarSupportedContent">
<ul className="navbar-nav ml-auto">
  <li className="nav-item">
    <a className="nav-link" href="/#features">Product</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="https://www.buymeacoffee.com/cinemaloop" target="_blank" rel="noopener noreferrer">Buy us popcorn</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="/#map">About us</a>
  </li>
  <li className="nav-item">
    <a href="/#" className="nav-link" data-toggle="modal" data-target="#contact-form">Contact</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" href="https://blog.cinemaloop.co/">blog</a>
  </li>
</ul>
 
  <button className="btn btn-dark" type="button" data-toggle="modal" data-target="#myModal">
      Early Access</button>
</div>
</nav>
  </div>
    );
  }
}

export default Navigation;
