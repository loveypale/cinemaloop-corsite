import React, { Component } from 'react';
import './App.css';
import './index.css';
import mock_1 from './app_mock.png';
import phone from './Phone_mockup.png';
class A extends Component {
  render() {
    return (
          <div class='row prod-features'>
              <div class='col-lg-12 prod-header'>
                <h2>Meet new movie fans anywhere you are!</h2>
                <h4 class='sub-header'>Join a community of like-minded moviegoers that share your passions and interests.</h4>
              </div>
            <p id="features"></p>
            <div id="left" class="col-sm">
              <div class="feat">
                <i class="fas fa-users fa-2x mb-2"></i>
                <h4>Make friends</h4>
                <p>Get matched with local cinemaloopers wanting to see the same movies, send movie requests, connect, chat and meet up with a new friend at the cinema.</p>
              </div>
              <div class="col feat_A">
              <i class="fas fa-ticket-alt fa-2x mb-2"></i>
                <h4>Portal to awesomeness</h4>
                <p>Get exclusives that no one else can access: personalized moviegoer community maps of fun movie-outings, pre and most movie easter eggs, director messages and more.</p>
              </div>
              <div class="feat_B">
              <i class="fas fa-map-marker-alt fa-2x mb-2"></i>
                <h4>Find theater locations</h4>
                <p>See showtimes. Check venue accessibilities. Buy tickets and get that perfect reserved seat.</p>
              </div>
            </div>

            <div id="right" class="col-sm">
              <img src={mock_1} class="app-mock" alt="app mockup"></img>
              <p class='CTA'>REQUEST EARLY ACCESS AND GET AN EXCLUSIVE INVITATION TO OUR 'CLOSED BETA CLUB'</p>
              <button type="button" class="btn btn-light my-2 my-sm-0" data-toggle="modal" data-target="#myModal">Early Access</button>
            </div>
            <div class='col-lg-12 features-exp'>
            <img src={phone} class="phone-mock" alt="app mockup"></img>
              <div id="features-tile">
              <h4>An all-round cinema experience</h4>  
              <p>Get access to exciting opportunities to enhance your movie going experience and make your time at the cinema more enjoyable.</p>
                
              <p>Read and review community user reviews and ratings of cinemas, and get the inside scoop on venues, eateries, locations, movies and much more.</p>
              <button type="button" class="btn btn-outline-dark video-but-2"  data-toggle="modal" data-target="#video_1">
            <i class="fas fa-play mr-2"></i>
            WATCH VIDEO</button>
              </div>
            </div>
          </div>
    );
  }
}

export default A;
