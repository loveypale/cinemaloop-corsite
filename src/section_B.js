import React, { Component } from 'react';
import './App.css';
import './index.css';
import { img01, img02, img03, img04, img05, img06, vera } from './';
class A extends Component {
  render() {
    return (
      <div class='row'>
        <p id="crew"></p>
          <div  class="col-sm-12 elgrupo">
              <h2>Connecting people over the joy of Cinema</h2>
              <br/>
              <p>We are an organic group of movie-loving misfits, moviebuffs, filmnerds, cinema-socialities, fandomites and cinephiles — side hustling together on a mission to design new innovative platform experiences for moviegoers. <a class="p-nav" href="https://angel.co/company/cinemaloop/jobs">Come Join Us!</a></p>
              <div class="cards-tainer">
                  <div class="card team-cards">
                    <div class="card-body">
                      <img class="col-image" src={img02} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>Tom</strong>&nbsp;-&nbsp; Board</div>
              </div>
              <div class="card team-cards">
                <div class="card-body">
                  <img class="col-image" src={img01} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>Vera</strong>&nbsp;-&nbsp; Founder,CVO</div>
              </div>
              <div class="card team-cards">
                <div class="card-body">
                  <img class="col-image" src={img03} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>Vera</strong>&nbsp;-&nbsp; Founder,CVO</div>
              </div>
              <div class="card team-cards">
                <div class="card-body">
                  <img class="col-image" src={img04} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>Vera</strong>&nbsp;-&nbsp; Founder,CVO</div>
              </div>
              <div class="card team-cards">
                <div class="card-body">
                  <img class="col-image" src={img05} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>David</strong>&nbsp;-&nbsp; Senior Branding Designer
</div>
              </div>
              <div class="card team-cards">
                <div class="card-body">
                  <img class="col-image" src={img06} alt="team member" />
                </div>
                <div class="card-footer">
                  <strong>Marisa</strong>&nbsp;-&nbsp; Social media director</div>
              </div>
          </div>
        </div>
        <div class='boss-lady'>
          <div class='avatar'>
            <p><img class="boss-avatar" src={vera} alt="team member" /></p>
            <p><strong>Vera</strong></p>
          </div> 
          <div class='bubble-wrap'>
            <div class='bubble'>
            Cinemaloop Inc. is a woman-minority-disabled owned startup that is funded through friends, family, a 'plan-to-achieve-self-support' grant through SSA.gov and donations from moviegoers like you.

            If you love what we are doing, please consider buying the team a bucket of popcorn, so we can get Cinemaloop into your hands.
            </div>
            <a href="https://www.buymeacoffee.com/cinemaloop" role="button" class="btn btn-dark access last-but my-2 my-sm-0" target="_blank" rel="noopener noreferrer">Buy us popcorn<i class="fas fa-chevron-right ml-2"></i></a>
          </div>
          
        </div>
      </div>

      );
  }
}

export default A;
